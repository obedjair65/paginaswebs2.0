function arribaMouse() {
    //Obtener el objeto
    let parrafo = document.getElementById('parrafo');
    parrafo.style.color = 'red';
    parrafo.style.fontSize = '15px';
    parrafo.innerHTML += "Hola Mundo";
}

function salir() {
    let parrafo = document.getElementById('parrafo');
    parrafo.style.color = 'green';
    parrafo.style.fontSize = '10px';
}

const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function calcular() 
{
    let peso = document.getElementById('peso').value;
    let altura = document.getElementById('altura').value;

    let imc = peso / (altura * altura);
    document.getElementById('resultado').value = imc;
});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function limpiar() {
    document.getElementById('altura').value = "";
    document.getElementById('peso').value = "";
    document.getElementById('resultado').value = "";
});
