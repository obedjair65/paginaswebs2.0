document.addEventListener("DOMContentLoaded", function() {
    const btnCalcular = document.getElementById("btnCalcular");
    const btnLimpiar = document.getElementById("btnLimpiar");

    btnCalcular.addEventListener("click", function() {
        calcularCotizacion();
    });

    btnLimpiar.addEventListener("click", function() {
        limpiar();
    });

    function calcularCotizacion() {
        const descripcion = document.getElementById("descripcion").value;
        const precio = parseFloat(document.getElementById("idPrecio").value);
        const porcentajePagoInicial = parseFloat(document.getElementById("idPorcentaje").value);
        const plazo = parseInt(document.getElementById("idPlazo").value);

        const pagoInicial = precio * (porcentajePagoInicial / 100);
        const totalFinanciar = precio - pagoInicial;
        const pagoMensual = totalFinanciar / plazo;

        document.getElementById("idPagoInicial").value = pagoInicial.toFixed(2);
        document.getElementById("idTotalFin").value = totalFinanciar.toFixed(2);
        document.getElementById("idPagoMensual").value = pagoMensual.toFixed(2);
    }

    function limpiar() {
        document.getElementById("descripcion").value = "";
        document.getElementById("idPrecio").value = "";
        document.getElementById("idPorcentaje").value = "";
        document.getElementById("idPlazo").value = "12";
        document.getElementById("idPagoInicial").value = "";
        document.getElementById("idTotalFin").value = "";
        document.getElementById("idPagoMensual").value = "";
    }
});
